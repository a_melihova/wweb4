<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
   // Массив для временного хранения сообщений пользователю.
	$messages = array();
  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
	  // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
	// Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
 }

   // Складываем признак ошибок в массив.
 $errors=array();
 $errors['name']=!empty($_COOKIE['name_error']);	
 $errors['mail']=!empty($_COOKIE['mail_error']);
 //$errors['year'] = !empty($_COOKIE['year_error']);
 $errors['sex']=!empty($_COOKIE['sex_error']);
 $errors['countlimbs']=!empty($_COOKIE['countlimbs_error']);
 $errors['super']=!empty($_COOKIE['super_error']);
 $errors['biography']=!empty($_COOKIE['biography_error']);
 $errors['check1']=!empty($_COOKIE['check1_error']);
// Выдаем сообщения об ошибках.
 if ($errors['name']) {
	 // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000); //в скобках пишем (название cookie, значение cookie, время)
	   // Выводим сообщение.
    $messages[] = '<div class="error">Вы не заполнили имя.</div>';
  }

  if ($errors['mail']) {
    setcookie('mail_error', '', 100000);
    $messages[] = '<div class="error">Корректно заполните email.</div>';
  }

 if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $messages[] = '<div class="error">Укажите пол.</div>';
  }

 if ($errors['countlimbs']) {
    setcookie('countlimbs_error', '', 100000);
    $messages[] = '<div class="error">Укажите количество конечностей.</div>';
  }

 if ($errors['super']) {
    setcookie('super_error', '', 100000);
    $messages[] = '<div class="error">Укажите сверхспособность.</div>';
  }

 if ($errors['biography']) {
    setcookie('biography_error', '', 100000);
    $messages[] = '<div class="error">Напишите биографию.</div>';
  }

 if ($errors['check1'])  {
     setcookie('check1_error', '', 100000);
     $messages[]= '<div class="error">Примите соглашение.</div>';
 }
// Складываем предыдущие значения полей в массив, если есть.
 $values = array();
//Если $_COOKIE['name_value'] пустое,то значение будет '', иначе $_COOKIE['name_value']
 $values['name']=empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
 $values['mail']=empty($_COOKIE['mail_value']) ? '' : $_COOKIE['mail_value'] ;
 $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
 $values['sex']=empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
 $values['countlimbs']=empty($_COOKIE['countlimbs_value']) ? '' : $_COOKIE['countlimbs_value'];
 $values['super']=empty($_COOKIE['super_value']) ? '' : $_COOKIE['super_value'];
 $values['biography']=empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
 //$values['check1']=empty($_COOKIE['check1_value'] ? '' : $_COOKIE['check1_value']);
 // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
 include('forma.php');

}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
	$errors = FALSE;
  
	if (empty($_POST['name']) || preg_match('/[^(\x7F-\xFF)|(\s)]/', $_POST['name'])) {
		    // Выдаем куку на день с флажком об ошибке в поле fio.
	  setcookie('name_error', '1', time() + 24 * 60 * 60);
	  $errors = TRUE;
	}
  
	else {
		  // Сохраняем ранее введенное в форму значение на месяц.
	  setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
	}
  
  
	if (empty($_POST['mail'])){
	  setcookie('mail_error', '1', time() + 24 * 60 * 60);
	  $errors=TRUE;
	}
  
	else{
	  setcookie('mail_value', $_POST['mail'], time()+ 30 * 24 * 60 * 60);
	}
  
	if (empty($_POST['sex'])){
	  setcookie('sex_error', '1', time() + 24 * 60 * 60);
	  $errors=TRUE;
	}
  
	else{
	  setcookie('sex_value', $_POST['sex'], time()+ 30 * 24 * 60 * 60);
	}
  
  
	if (empty($_POST['countlimbs'])){
	  setcookie('countlimbs_error', '1', time() + 24 * 60 * 60);
	  $errors=TRUE;
	}
  
	else{
	  setcookie('countlimbs_value', $_POST['countlimbs'], time()+ 30 * 24 * 60 * 60);
	}
  
  
   if (empty($_POST['super'])){
	   setcookie('super_error', '1', time() + 24 * 60 * 60);
	   $errors=TRUE;
	 }
	  else{
	  $super=implode(' , ',$_POST['super']);
	  setcookie('super_value', $super, time()+ 30 * 24 * 60 * 60);
	  }
	  
  
	if (empty($_POST['biography'])){
	  setcookie('biography_error', '1', time() + 24 * 60 * 60);
	  $errors=TRUE;
	}
  
	else{
	  setcookie('biography_value', $_POST['biography'], time()+ 30 * 24 * 60 * 60);
	}
  
  if(empty($_POST['check1'])){
		  setcookie('check1_error', '1', time()+ 24 * 60 * 60);
		  $errors=TRUE;
	  }
  
	  else {
		  setcookie('check1_value', $_POST['check1'], time()+30 * 24 * 60 * 60);
	  }
	  
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.  
	if ($errors) {
	  header('Location: index.php');
	  exit();
	}
	else {
		// Удаляем Cookies с признаками ошибок.
	  setcookie('name_error', '', 100000);
	  setcookie('mail_error', '', 100000);
	  setcookie('sex_error', '', 100000);
	  setcookie('countlimbs_error', '', 100000);
	  setcookie('super_error', '', 100000);
	  setcookie('biography_error', '', 100000);
	  setcookie('check1_error', '', 100000);
	}
  
	$user = 'u24014';
	$pass = '6041366';
	$db = new PDO('mysql:host=localhost;dbname=u24014', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

	  try
	  {
		$str=implode(' , ',$_POST['super']); 

		  $stmt = $db->prepare("INSERT INTO application (name, mail, year, sex, countlimbs, super, biography) VALUES (:name, :mail, :year, :sex, :countlimbs, :super, :biography)");
		  $stmt->bindParam(':name', $name); //Привязка переменных к параметрам подготавливаемого запроса 
		  $stmt->bindParam(':mail', $mail);
		  $stmt->bindParam(':year', $year);
		  $stmt->bindParam(':sex', $sex);
		  $stmt->bindParam(':countlimbs', $countlimbs);
		  $stmt->bindParam(':super', $super);
		  $stmt->bindParam(':biography', $biography);
		  $name_db=$_POST["name"];
		  $mail_db=$_POST["mail"];
		  $year_db=$_POST["year"];
		  $sex_db=$_POST["sex"];
		  $limb_db=$_POST["countlimbs"];
		  $super_db=$str;
		  $bio_db=$_POST["biography"];
		  $stmt->execute();
		  print ('Результаты отпралены!');
		  exit();
	  }
	  catch(PDOException $e)
	  {
		  print ('Error: ' . $e->getMessage());
		  exit();
	  }
  
  
  // Сохраняем куку с признаком успешного сохранения.  
	setcookie('save', '1');
	  // Делаем перенаправление.
	header('Location: index.php');
  }


?>
